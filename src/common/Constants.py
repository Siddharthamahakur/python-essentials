import os

PROJECT_DIR = os.path.dirname(os.path.realpath(__file__)).rsplit(os.sep, 2)[0]
# MODE_LIST = ['PERMISSIVE', 'DROPMALFORMED', 'FAILFAST']
MODE_LIST = ['PERMISSIVE', 'DROPMALFORMED']
